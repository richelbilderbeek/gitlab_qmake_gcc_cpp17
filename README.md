# gitlab_qmake_gcc_cpp17

Branch   |GitLab CI
---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
`master` |[![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/commits/master)
`develop`|[![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/badges/develop/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/commits/develop)

Minimal project that uses qmake, GCC, C++17 and is tested by GitLab

This GitHub is part of [the GitLab C++ Tutorial](https://github.com/richelbilderbeek/gitlab_cpp_tutorial).

The goal of this project is to have a clean GitLab CI build, with specs:

 * Build system: `qmake`
 * C++ compiler: `gcc`
 * C++ version: `C++17`
 * Libraries: `STL` only
 * Code coverage: none
 * Source: one single file, `main.cpp`

More complex builds:

[![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/commits/master)

 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp20/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp20) Use C++20: [gitlab_qmake_gcc_cpp20](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp20)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_bpp/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_bpp) Add `Bio++`: [gitlab_qmake_gcc_cpp17_bpp](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_bpp)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost) Add `Boost`: [gitlab_qmake_gcc_cpp17_boost](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost_test/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost_test) Add `Boost.Test`: [gitlab_qmake_gcc_cpp17_boost_test](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_boost_test)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-format/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-format) [gitlab_qmake_gcc_cpp17_clang-format](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-format)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy) [gitlab_qmake_gcc_cpp17_clang-tidy](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_codechecker/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_codechecker) Add CodeChecker: [gitlab_qmake_gcc_cpp17_codechecker](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_codechecker)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_doxygen/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_doxygen) Add Doxygen: [gitlab_qmake_gcc_cpp17_doxygen](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_doxygen)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gcov/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gcov) Add code coverage: [gitlab_qmake_gcc_cpp17_gcov](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gcov)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy) Add `clang-tidy`: [gitlab_qmake_gcc_cpp17_clang-tidy](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_clang-tidy)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_cppcheck/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_cppcheck) Add `cppcheck`: [gitlab_qmake_gcc_cpp17_cppcheck](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_cppcheck)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_fltk/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_fltk) Add `FLTK`: [gitlab_qmake_gcc_cpp17_fltk](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_fltk)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gprof/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gprof) Add `gprof`: [gitlab_qmake_gcc_cpp17_gprof](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_gprof)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_magic_enum/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_magic_enum) Add `magic_enum`: [gitlab_qmake_gcc_cpp17_magic_enum](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_magic_enum)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint) Add `OCLint`: [gitlab_qmake_gcc_cpp17_oclint](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_oclint)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_perf/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_perf) Add `perf`: [gitlab_qmake_gcc_cpp17_perf](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_perf)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_qt/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_qt) Add `Qt`: [gitlab_qmake_gcc_cpp17_qt](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_qt)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_r/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_r) Add `R`: [gitlab_qmake_gcc_cpp17_r](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_r)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sdl/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sdl) Add `SDL`: [gitlab_qmake_gcc_cpp17_sdl](https://github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sdl)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sfml/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sfml) Add `SFML`: [gitlab_qmake_gcc_cpp17_sfml](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_sfml)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_rcpp/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_rcpp) Add `Rcpp`: [gitlab_qmake_gcc_cpp17_rcpp](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_rcpp)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_urho3d/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_urho3d) Add `Urho3D`: [gitlab_qmake_gcc_cpp17_urho3d](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_urho3d)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_wt/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_wt) Add `Wt`: [gitlab_qmake_gcc_cpp17_wt](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_wt)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_xmlpp/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_xmlpp) Add `xml++`: [gitlab_qmake_gcc_cpp17_xmlpp](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_xmlpp)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdc/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdc) Travis-dependent compilation: [gitlab_qmake_gcc_cpp17_tdc](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdc)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdr/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdr) Travis-dependent run: [gitlab_qmake_gcc_cpp17_tdr](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp17_tdr)

Builds of similar complexity:

 * [![Build Status](https://travis-ci.org/richelbilderbeek/travis_qmake_gcc_cpp17.svg?branch=master)](https://travis-ci.org/richelbilderbeek/travis_qmake_gcc_cpp17) Use Travis CI: [travis_qmake_gcc_cpp17](https://www.github.com/richelbilderbeek/travis_qmake_gcc_cpp17)
 * [![pipeline status](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/badges/master/pipeline.svg)](https://gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17/commits/master) Use GitLab CI: [gitlab_qmake_gcc_cpp17](https://www.gitlab.com/richelbilderbeek/gitlab_qmake_gcc_cpp17)
 * ![GitHub Actions](https://github.com/richelbilderbeek/gha_qmake_gcc_cpp17/workflows/check/badge.svg?branch=master) Use GitHub Actions: [gha_qmake_gcc_cpp17](https://www.github.com/richelbilderbeek/gha_qmake_gcc_cpp17)

Less complex builds:

 * Use C++98: [gitlab_qmake_gcc_cpp98](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp98)
 * Use C++11: [gitlab_qmake_gcc_cpp11](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp11)
 * Use C++14: [gitlab_qmake_gcc_cpp14](https://www.github.com/richelbilderbeek/gitlab_qmake_gcc_cpp14)
