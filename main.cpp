#include <iostream>

int main()
{
  static_assert("C++17"); //C++17 has a default message
  std::cout << "Hello C++17\n";
}
